<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="container top-container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Добрый день!</h3>
            <h4>Заполните форму подписки</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <c:if test="${success eq true}">
                <div class="alert alert-success">Вы успешно подписаны</div>
            </c:if>

            <c:if test="${error eq true}">
                <div class="alert alert-danger">Вы уже подписаны</div>
            </c:if>

            <h4>Добавление статьи</h4>

            <form action="<%=ClientRoutes.ROOT%>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="input-title">Заголовок</label>
                    <input name="title" type="text" class="form-control" id="input-title" required="true"/>
                </div>

                <div class="form-group">
                    <label for="textarea-desc">Описание</label>
                    <textarea class="form-control" rows="3" name="desc" id="textarea-desc"></textarea>
                </div>

                <div class="form-group">
                    <label for="input-pic">Прикрепить изображение</label>
                    <input name="pic" type="file" id="input-pic" required="true"/>
                    <p class="help-block">Example block-level help text here.</p>
                </div>

                <button type="submit" class="btn btn-default">Добавить</button>
            </form>
        </div>
    </div>
</div>
