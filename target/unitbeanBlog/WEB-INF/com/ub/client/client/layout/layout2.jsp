<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html>
<html>
<head>
    <title>Добро пожаловать!</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="/static/blog/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<c:url value="/static/a/css/font-icons/entypo/css/entypo.css"/>">
    <link rel="stylesheet" href="/static/blog/css/custom.css"/>
</head>
<body>
<header>
    <div class="container">
        <div class="header__logo">
            <a href="#" class="logo">
                <img src="/static/blog/images/zeplin/logo.png"/
                >
                <span>UnitBean</span>
            </a>
        </div>

        <a href="<%=ClientRoutes.ADD%>" class="header__addlink">Добавить статью</a>

        <h1>Статьи, 24</h1>

    </div>
</header>

<div class="container">
    <nav class="navbar">
        <div class="container">

            <div class="navbar-header">
                <div class="row">
                    <a href="<%=ClientRoutes.ROOT%>" class="navbar-brand">
                        <img src="/static/blog/images/zeplin/logo.png" alt=""/>
                        <span class="logo-text">UnitBean</span>
                    </a>
                </div>

                <div class="row">
                    <h1>Статьи, 24</h1>
                </div>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav nav-list">
                    <li>
                        <a href="<%=ClientRoutes.ROOT%>" class="add-link btn btn-default navbar-right" role="button">
                            Добавить статью
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<tiles:insertAttribute name="content"/>
</body>
</html>

