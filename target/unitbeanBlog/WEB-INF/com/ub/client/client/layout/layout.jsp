<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html>
<html>
<head>
    <title>Добро пожаловать!</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="/static/blog/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<c:url value="/static/a/css/font-icons/entypo/css/entypo.css"/>">
    <link rel="stylesheet" href="/static/blog/css/fonts.css"/>
    <link rel="stylesheet" href="/static/blog/css/custom.css"/>
</head>
<body>

    <header>
        <div class="container">
            <div class="header__logo">
                <a href="<%=ClientRoutes.ROOT%>" class="logo">
                    <img src="/static/blog/images/zeplin/logo.png">
                    <span>UnitBean</span>
                </a>
            </div>

            <a href="<%=ClientRoutes.ADD%>" class="header__addlink">Добавить статью</a>

            <h1 class="header__title">Статьи, 24</h1>

        </div>
    </header>

    <tiles:insertAttribute name="content"/>

    <script type="text/javascript" src="/static/blog/js/jquery/jquery-3.2.1.js"></script>
    <script src="/static/blog/js/custom.js"></script>
</body>
</html>
