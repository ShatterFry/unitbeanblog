<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:if test="${not empty clientDoc.id}">
    <form:hidden path="id"/>
</c:if>

<div class="row form-title">
    <div class="col-lg-12">
        <label for="title" class="form-label">Заголовок</label>
        <form:input path="title" cssClass="form-control" id="title"/>
    </div>
</div>

<div class="row form-desc">
    <div class="col-lg-12">
        <label for="desc" class="form-label">Описание</label>
        <form:textarea path="desc" cssClass="form-control" id="desc" rows="11"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="add__image">
            <label for="picFile" class="file-input-label">Прикрепить изображение</label>

            <c:if test="${not empty postDoc.pic}">
                <form:hidden path="pic" cssClass="form-control" id="pic"/>
            </c:if>

            <div class="add__image-input">
                <input type="file" name="picFile" id="picFile"/>
                <img src="/static/blog/images/zeplin/photo-icon.png" alt=""/>
            </div>
        </div>

        <button class="btn btn-success btn-lg pull-right submit__button" type="submit">Добавить</button>

        <a href="<%=ClientRoutes.ROOT%>" class="btn btn-default btn-lg pull-right cancel__link">
            <span class="cancel__link-text">Отмена</span>
        </a>

        <c:if test="${not empty postDoc.pic}">
            <img src="/pics/${postDoc.pic}" style="max-width: 100%;"/>
        </c:if>
    </div>
</div>
