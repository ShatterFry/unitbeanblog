<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:if test="${not empty clientDoc.id}">
    <form:hidden path="id"/>
</c:if>

<div class="row">
    <div class="col-lg-12">
        <label for="title">Заголовок</label>
        <form:input path="title" cssClass="form-control" id="title"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="desc">Описание</label>
        <form:textarea path="desc" cssClass="form-control" id="desc" rows="3"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="picFile">Прикрепить изображение</label>

        <c:if test="${not empty postDoc.pic}">
            <form:hidden path="pic" cssClass="form-control" id="pic"/>
        </c:if>

        <input name="picFile" type="file" id="picFile"/>

        <c:if test="${not empty postDoc.pic}">
            <img src="/pics/${postDoc.pic}" style="max-width: 100%;"/>
        </c:if>
    </div>
</div>
