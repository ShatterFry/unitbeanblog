<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Congratulation</title>

    <link rel="stylesheet" href="/static/blog/css/fonts.css"/>
    <link rel="stylesheet" href="/static/blog/css/congratulation.css" type="text/css"/>
</head>

    <body>

        <div class="wrapper">
            <img src="/static/blog/images/zeplin/congr.png">
            <span>Поздравляем Вас с успешным добавлением статьи в блог UnitBean!</span>
        </div>

    </body>

</html>
