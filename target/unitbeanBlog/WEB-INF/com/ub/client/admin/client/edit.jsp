<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="container top-container">
    <div class="row">
        <div class="col-md-12">
            <h1>Редактирование</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form:form action="<%=ClientRoutes.EDIT%>" method="post" modelAttribute="postDoc" enctype="multipart/form-data">
                <%--<c:set var="postDoc" value="${postDoc}" scope="request"/>--%>
                <jsp:include page="form.jsp"/>
                <br/>
                <button class="btn btn-success" type="submit">Добавить</button>
            </form:form>
        </div>
    </div>
</div>
