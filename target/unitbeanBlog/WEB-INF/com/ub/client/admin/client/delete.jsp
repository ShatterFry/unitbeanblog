<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="container top-container">
    <div class="row">
        <div class="col-md-12">
            <h1>Подтвердите удаление поста</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form action="<%=ClientRoutes.DELETE%>" method="post">
                <input type="hidden" name="id" value="${id}"/>
                <button type="submit" class="btn btn-danger">Да, удалить</button>
            </form>
        </div>
    </div>
</div>
