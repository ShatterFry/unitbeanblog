<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="container top-container">
    <div class="row">
        <div class="col-md-12"></div>
    </div>

    <div class="widget widget-blue">
        <div class="widget-title">
            <h3>Результаты поиска:</h3>
        </div>

        <div class="widget-content">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>Дата</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${searchPostResponse.result}" var="postDoc">
                            <tr>
                                <td>${postDoc.title}</td>
                                <td>${postDoc.formattedDate}</td>
                                <td>${postDoc.desc}</td>
                                <td>
                                    <c:url value="<%=ClientRoutes.EDIT%>" var="urlEdit">
                                        <c:param name="id" value="${postDoc.id}"></c:param>
                                    </c:url>
                                    <a href="${urlEdit}" class="btn btn-default btn-xs">Редактирование</a>

                                    <c:url value="<%=ClientRoutes.DELETE%>" var="urlDelete">
                                        <c:param name="id" value="${postDoc.id}"></c:param>
                                    </c:url>
                                    <a href="${urlDelete}" class="btn btn-danger btn-xs">Удаление</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="pagination pagination-sm">
                <c:url value="<%=ClientRoutes.ALL%>" var="urlPrev">
                    <c:param name="currentPage" value="${searchPostResponse.prevNum()}"></c:param>
                </c:url>
                <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

                <c:forEach items="${searchPostResponse.paginator()}" var="page">
                    <c:url value="<%=ClientRoutes.ALL%>" var="url">
                        <c:param name="currentPage" value="${page}"/>
                    </c:url>
                    <li>
                        <a href="${url}"
                       class="<c:if test="${searchPostResponse.currentPage eq page}">active</c:if>">
                            ${page+1}
                        </a>
                    </li>
                </c:forEach>

                <c:url value="<%=ClientRoutes.ALL%>" var="urlNext">
                    <c:param name="currentPage" value="${searchPostResponse.nextNum()}"></c:param>
                </c:url>
                <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
            </ul>
        </div>
    </div>
</div>