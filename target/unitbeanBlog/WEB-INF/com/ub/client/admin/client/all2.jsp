<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="container top-container">

    <section class="content-wrapper">
        <div class="container">
            <main class="content">
                <ul class="posts">
                    <c:forEach items="${searchPostResponse.result}" var="postDoc">
                        <li class="posts__item">

                                <img class="post__link-img" src="/pics/${postDoc.pic}" alt=""/>
                                <a href="#"><h2 class="post__link-title">${postDoc.title}</h2></a>
                                <span class="post__link-date">${postDoc.formattedDate}</span>
                                <div class="post__link-borderline"></div>
                                <span class="post__link-descr">${postDoc.desc}</span>

                        </li>
                    </c:forEach>
                </ul>
            </main>
        </div>
    </section>

<div class="row">
        <div class="col-md-12 text-center">
            <ul class="pagination pagination-sm">
                <c:url value="<%=ClientRoutes.ALL%>" var="urlPrev">
                    <c:param name="currentPage" value="${searchPostResponse.prevNum()}"></c:param>
                </c:url>
                <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i></a></li>

                <c:forEach items="${searchPostResponse.paginator()}" var="page">
                    <c:url value="<%=ClientRoutes.ALL%>" var="url">
                        <c:param name="currentPage" value="${page}"/>
                    </c:url>
                    <li>
                        <a href="${url}"
                           class="<c:if test="${searchPostResponse.currentPage eq page}">active</c:if>">
                                ${page+1}
                        </a>
                    </li>
                </c:forEach>

                <c:url value="<%=ClientRoutes.ALL%>" var="urlNext">
                    <c:param name="currentPage" value="${searchPostResponse.nextNum()}"></c:param>
                </c:url>
                <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i></a></li>
            </ul>
        </div>
    </div>
</div>