package com.ub.unitbeanBlog.client.services;

import com.ub.core.picture.services.PictureService;
import com.ub.unitbeanBlog.client.models.PostDoc;
import com.ub.unitbeanBlog.client.services.exception.PostExistException;
import com.ub.unitbeanBlog.client.view.SearchPostRequest;
import com.ub.unitbeanBlog.client.view.SearchPostResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class PostService {
    @Autowired private MongoTemplate mongoTemplate;
    @Autowired private PictureService pictureService;

    public PostDoc create(String title, String desc, MultipartFile pic)
            throws PostExistException{
        PostDoc postDoc = findByTitle(title);
        if(postDoc != null){
            throw new PostExistException();
        }

        postDoc = new PostDoc();
        postDoc.setTitle(title);

        Date date = new Date();
        postDoc.setDate(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM','y");
        postDoc.setFormattedDate(dateFormat.format(date));

        postDoc.setDesc(desc);
        ObjectId picId = pictureService.saveWithDelete(pic, null);
        postDoc.setPic(picId);

        return save(postDoc);
    }

    public PostDoc findById(ObjectId id){
        return mongoTemplate.findById(id, PostDoc.class);
    }

    public PostDoc findByTitle(String title){
        Criteria criteria = new Criteria().where("title").is(title);
       return mongoTemplate.findOne(new Query(criteria), PostDoc.class);
    }

    public PostDoc save(PostDoc postDoc){
        mongoTemplate.save(postDoc);
        return postDoc;
    }

    public void delete(ObjectId id){
        PostDoc postDoc = findById(id);
        mongoTemplate.remove(postDoc);
    }

    public SearchPostResponse findAll(SearchPostRequest searchPostRequest){
        Sort sort = new Sort(Sort.Direction.ASC, "date");
        Pageable pageable = new PageRequest(
                searchPostRequest.getCurrentPage(),
                searchPostRequest.getPageSize(),
                sort
        );

        Criteria criteria = new Criteria();
        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, PostDoc.class);

        query = query.with(pageable);

        List<PostDoc> results = mongoTemplate.find(query, PostDoc.class);

        SearchPostResponse response = new SearchPostResponse(
                searchPostRequest.getCurrentPage(),
                searchPostRequest.getPageSize(),
                results
        );
        response.setAll(count);
        return response;
    }
}
