package com.ub.unitbeanBlog.client.routes;

public class ClientRoutes {
    public static final String ROOT = "/";
    public static final String ADD = ROOT + "add";
    public static final String EDIT = ROOT + "edit";
    public static final String ALL = ROOT + "all";
    public static final String DELETE = ROOT + "delete";
    public static final String CONGR = ROOT + "congratulation";
}
