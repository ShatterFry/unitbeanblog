package com.ub.unitbeanBlog.client.view;

import com.ub.core.base.search.SearchRequest;

public class SearchPostRequest extends SearchRequest{
    public SearchPostRequest() {

    }

    public SearchPostRequest(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
