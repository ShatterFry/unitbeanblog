package com.ub.unitbeanBlog.client.view;

import com.ub.core.base.search.SearchResponse;
import com.ub.unitbeanBlog.client.models.PostDoc;

import java.util.List;

public class SearchPostResponse extends SearchResponse{
    private List<PostDoc> result;

    public SearchPostResponse(Integer currentPage, Integer pageSize, List<PostDoc> result) {
        this.result = result;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
    }

    public SearchPostResponse(Integer currentPage, List<PostDoc> result) {
        this.result = result;
        this.currentPage = currentPage;
    }

    public List<PostDoc> getResult() {
        return result;
    }

    public void setResult(List<PostDoc> result) {
        this.result = result;
    }
}
