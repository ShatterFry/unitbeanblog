package com.ub.unitbeanBlog.client.controllers;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.services.PictureService;
import com.ub.unitbeanBlog.client.models.PostDoc;
import com.ub.unitbeanBlog.client.routes.ClientRoutes;
import com.ub.unitbeanBlog.client.services.PostService;
import com.ub.unitbeanBlog.client.services.exception.PostExistException;
import com.ub.unitbeanBlog.client.view.SearchPostRequest;
import com.ub.unitbeanBlog.client.view.SearchPostResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClientController {
    @Autowired private PostService postService;
    @Autowired private PictureService pictureService;

    @RequestMapping(value = ClientRoutes.ADD, method = RequestMethod.GET)
    public String add(Model model){
        PostDoc postDoc = new PostDoc();
        model.addAttribute("postDoc", postDoc);
        return "com.ub.client.add";
    }

    @RequestMapping(value = ClientRoutes.ADD, method = RequestMethod.POST)
    public String add(@ModelAttribute PostDoc postDoc,
                      @RequestParam MultipartFile picFile,
                      RedirectAttributes ra){
        try {
            postDoc = postService.create(postDoc.getTitle(), postDoc.getDesc(), picFile);
        } catch (PostExistException e) {
            PostDoc tmp = postService.findByTitle(postDoc.getTitle());
            ra.addAttribute("id", tmp.getId());
            return RouteUtils.redirectTo(ClientRoutes.EDIT);
        }
        //ra.addAttribute("id", postDoc.getId());
        return RouteUtils.redirectTo(ClientRoutes.CONGR);
    }

    @RequestMapping(value = ClientRoutes.ROOT, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage, Model model){
        SearchPostRequest searchPostRequest = new SearchPostRequest(currentPage);
        SearchPostResponse searchPostResponse = postService.findAll(searchPostRequest);
        model.addAttribute("searchPostResponse", searchPostResponse);
        return "com.ub.client.all";
    }

    @RequestMapping(value = ClientRoutes.CONGR, method = RequestMethod.GET)
    public String congr(){
        return "com.ub.client.congr";
    }
}
