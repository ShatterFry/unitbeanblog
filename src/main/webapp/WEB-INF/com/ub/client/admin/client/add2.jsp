<%@ page import="com.ub.unitbeanBlog.client.routes.ClientRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html>

<html>

    <head>
        <title>Добро пожаловать!</title>

        <meta charset="UTF-8"/>

        <link rel="stylesheet" href="/static/blog/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<c:url value="/static/a/css/font-icons/entypo/css/entypo.css"/>">
        <link rel="stylesheet" href="/static/blog/css/fonts.css"/>
        <link rel="stylesheet" href="/static/blog/css/custom.css"/>
        <link rel="stylesheet" href="/static/blog/css/add.css"/>
    </head>

    <body>

        <div class="container top-container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title">Добавление статьи</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form:form action="<%=ClientRoutes.ADD%>" method="post" modelAttribute="postDoc" enctype="multipart/form-data">
                        <jsp:include page="form2.jsp"/>
                    </form:form>
                </div>
            </div>
        </div>

    </body>

</html>
